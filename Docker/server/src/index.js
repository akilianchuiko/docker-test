const express = require("express")
const redis = require("redis");

const app = express()

app.get('/', function (req, res){
  res.send('Hello World')
})

app.listen(3000, () => {
  console.log('123')
  console.log('url', process.env.REDIS_URL)
});
console.log('test');
(async () => {

  const client = redis.createClient({
    url:'redis://redis'
  });
  client.on('error', (err) => console.error(err));

  await client.set("key", "value", redis.print);
  await client.get("key", redis.print);
})();